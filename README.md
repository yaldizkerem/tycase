This repository helps you to spin up a two-node kubernetes cluster and build a platform stack on/around it.

## Quick Start

### Prerequisites

Helm installed in your computer. See [https://helm.sh/docs/intro/install/](https://helm.sh/docs/intro/install/) for installation instructions.

This repository heavily depends on kubespray and includes it as a submodule.
```
# Clone with --recurse-submodules option
git clone --recurse-submodules git@github.com:yaldizkerem/tycase.git

# or execute submodule update --init
git submodule update --init
```

The whole deployment process is achieved with ansible components and depends on python. For keeping python environments clean and distinct from each other usage of python virtual environments is strongly suggested.
```
# Create a virtual environment and activate
python3 -m venv venv
source venv/bin/activate
```

The tools that are used have some python dependencies.
```
# Install python dependencies
pip install -r requirements.txt
```

This repository also heavily depends on various ansible roles and collections.
```
# Install dependent ansible roles and collections
ansible-galaxy install -r requirements.yml
```

The deployed components listen to predefined FQDNs for ease of use and the [fact.yml](fact.yml) playbook leverages the use of /etc/hosts aliases.
```
# Put /etc/hosts alias entries
ansible-playbook fact.yml -b -K
```

### Deployment

The whole deployment process is accomplished by the use of ansible playbooks.
```
# Spin up the kubernetes cluster with kubespray
ansible-playbook --become --become-user=root kubespray/cluster.yml

# Deploy ingress-nginx chart
ansible-playbook nginx-ingress.yml

# Deploy prometheus operator chart
ansible-playbook prometheus-server.yml

# Install consul server
ansible-playbook consul.yml

# Install alertmanager
ansible-playbook alertmanager.yml

# Deploy a headless service and an endpoint to point the external prometheus
# and insert an /etc/hosts alias to point the internal prometheus
ansible-playbook prometheus-host-aliases.yml

# Install prometheus server
ansible-playbook external-prometheus-server.yml

# Deploy grafana chart
ansible-playbook grafana.yml

# Install elasticsearch and kibana and deploy fluent-bit chart
ansible-playbook efk.yml

# Install gitLab
ansible-playbook gitlab.yml

# Deploy cert-manager chart
ansible-playbook cert-manager.yml

# Deploy custom validating webhook charts
ansible-playbook custom-validating-webhook.yml
```

## Quick Links

To be able to use the links below you must run [fact.yml](fact.yml) playbook or manually insert alias entries to the `/etc/hosts/`.

Ingress Controller's Service type is NodePort. HTTP port is set to 32080 and HTTPS port is set to 32443

* Internal Prometheus: [http://prometheus.tycase.local:32080/](http://prometheus.tycase.local:32080/)
* External Prometheus: [http://external-prometheus.tycase.local:9090/](http://external-prometheus.tycase.local:9090/)
* Alertmanager: [http://alertmanager.tycase.local:9093/](http://alertmanager.tycase.local:9093/)
* Grafana: [http://grafana.tycase.local:32080/](http://grafana.tycase.local:32080/)
* Kibana: [http://efk.tycase.local:5601/](http://efk.tycase.local:5601/)
* Gitlab: [http://gitlab.tycase.local/](http://gitlab.tycase.local/)
* Gitlab project: [http://gitlab.tycase.local/root/tycase/-/tree/main](http://gitlab.tycase.local/root/tycase/-/tree/main)

### Current `/etc/hosts/` entries

```
8.208.14.34 prometheus.tycase.local
8.208.14.34 grafana.tycase.local
8.208.90.165 external-prometheus.tycase.local
8.208.90.165 alertmanager.tycase.local
8.208.89.103 efk.tycase.local
8.208.89.103 gitlab.tycase.local
```

### Credentials

* Grafana: username: `admin`, password: `tycaseadmin`
* Gitlab: username: `root`, password is stored in the server in `/etc/gitlab/initial_root_password` but [gitlab-runner.yml](gitlab-runner.yml) fetches it to `{{ inventory_dir }}/artifacts/gitlab_initial_root_password`

## Components

* [Kubespray](https://github.com/kubernetes-sigs/kubespray)
* [Nginx Ingress Controller](https://artifacthub.io/packages/helm/ingress-nginx/ingress-nginx)
* [Prometheus Server](https://artifacthub.io/packages/helm/prometheus-community/prometheus)
* [Alertmanager](https://github.com/cloudalchemy/ansible-alertmanager.git)
* [Consul](https://github.com/ansible-community/ansible-consul.git)
* [Grafana](https://artifacthub.io/packages/helm/grafana/grafana)
* [Elasticsearch](https://galaxy.ansible.com/geerlingguy/elasticsearch)
* [Kibana](https://galaxy.ansible.com/geerlingguy/kibana)
* [Fluentbit](https://artifacthub.io/packages/helm/fluent/fluent-bit)
* [Gitlab](https://galaxy.ansible.com/geerlingguy/gitlab)
* [Gitlab Runner](https://galaxy.ansible.com/riemers/gitlab-runner)
* [cert-manager](https://github.com/cert-manager/cert-manager)
* [Custom Validating Webhook](https://github.com/yaldizkerem/cpu-limit-cvw) or [Custom Validating Webhook](https://gitlab.com/yaldizkerem/cpu-limit-cvw)

Additional Ansible Roles and Collections can be found in [requirements.yml](requirements.yml).

## Playbooks and Their Purpose

[fact.yml](fact.yml): Puts `/etc/hosts` alias entries.

[kubespray/cluster.yml](kubespray/cluster.yml): Kubernetes cluster deployment playbook, see [Kubespray](https://github.com/kubernetes-sigs/kubespray) for additional information.

[nginx-ingress.yml](nginx-ingress.yml): Deploys `ingress-nginx` chart.

[prometheus-server.yml](prometheus-server.yml): Taints server1 and deploys `kube-prometheus-stack` chart.

[consul.yml](consul.yml): Installs Consul Server and registers a service for the Internal Prometheus.

[alertmanager.yml](alertmanager.yml): Installs Alertmanager and registers a Slack receiver.

[prometheus-host-aliases.yml](prometheus-host-aliases.yml): Inserts an `/etc/hosts` alias entry to point the Internal Prometheus. Also creates a Headless Service and an Endpoint to point the External Prometheus.

[external-prometheus-server.yml](external-prometheus-server.yml): Deploys Prometheus Server and registers Prometheus Pod restart alarm rule. Also configures Consul Service Discovery.

[grafana.yml](grafana.yml): Deploys `grafana` chart, loads a Grafana Dashboard and registers the External Prometheus as a datasource.

[efk.yml](efk.yml): Installs Elasticsearch and Kibana. Creates a Headless Service and an Endpoint to point the Elasticsearch instance. Also deploys `fluent-bit` chart.

[gitlab.yml](gitlab.yml): Installs Gitlab.

[gitlab-runner.yml](gitlab-runner.yml): Creates a Gitlab project which contains the repository. Installs Docker. Installs Gitlab Runner and registers it to the project.

[cert-manager.yml](cert-manager.yml): Deploys `cert-manager` chart.

[custom-validating-webhook.yml](custom-validating-webhook.yml): Deploys a custom validating webhook manifests files.
